# Technische tests

Je bent net begonnen bij bedrijf JavaWolf! Zij maken software om personen en adresgegevens te beheren. 

Jouw eerste project is code schrijven om de volgende use cases in een web service te implementeren:

1. Ophalen van alle personen.
1. Ophalen van een enkele persoon.
1. Zoeken op voornaam, achternaam, geboortedatum, postcode en huisnummer.
1. Kunnen toevoegen van een persoon met alle bijbehorende gegevens.
1. Kunnen wijzigen van een persoon met alle bijbehorende gegevens.
1. Kunnen verwijderen van een persoon met alle bijbehorende gegevens.

Vanuit je de organisatie komen de volgende eisen:

1. Je moet Java gebruiken, met als minimum versie 8.
1. Je moet een database gebruiken.
1. De Git log moet duidelijk leesbaar zijn.
1. De applicatie moet op elk systeem kunnen draaien. 

Wat randvoorwaarden voor de opdracht:

1. De tijdsindicatie is 3 uur. 
1. Je kan er vanuit gaat dat iedere developer Git, Maven, Gradle heeft geïnstalleerd.
1. Liever iets goed af dan alles half af.
1. Wij kijken of je code leesbaar is, te volgen is, en of jij kunt uitleggen waarom je bepaalde keuzes hebt gemaakt. Liever een matige keuze met een goed verhaal, dan een goede keuze met een slecht verhaal.
1. De initiële dataset kan je [hier als CSV vinden](dataset.csv).
